# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm config set registry https://registry.npmjs.org/ && \
    npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
WORKDIR /usr/share/nginx/html
COPY --from=build-stage /app/dist /app/*firebase* ./
RUN apk add --update nodejs npm && \
    npm install -g firebase-tools
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]